let unsorted = [46,23,63,19,48,74,27,54,34];

function quicksort() {
    let coreElem = 0;
    let less = [];
    let more = [];
    for (i=1; i<unsorted.length; i++) {
        if (unsorted[i]<unsorted[coreElem]) {
            less.push(unsorted[i]);
        } else {
            more.push(unsorted[i]);
        }
    }
    unsorted = less.concat(unsorted[coreElem], more);
    console.log(unsorted);
}
